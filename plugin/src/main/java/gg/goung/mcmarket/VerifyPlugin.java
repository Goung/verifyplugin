package gg.goung.mcmarket;

import gg.goung.mcmarket.command.WebRegisterCommand;
import gg.goung.mcmarket.database.SQLManager;
import gg.goung.mcmarket.database.redis.PluginSubscriber;
import gg.goung.mcmarket.database.redis.RedisManager;
import gg.goung.mcmarket.listener.ProfileListener;
import gg.goung.mcmarket.profile.ProfileManager;
import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.SQLException;

@Getter
public class VerifyPlugin extends JavaPlugin {
    @Getter private static VerifyPlugin instance;

    private YamlConfiguration messageConfig;
    private SQLManager sqlManager;
    private RedisManager redisManager;
    private ProfileManager profileManager;

    public void onEnable() {
        VerifyPlugin.instance = this;
        loadConfigs();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new ProfileListener(this), this);

        this.sqlManager = new SQLManager(this);
        this.redisManager = new RedisManager(this);
        this.profileManager = new ProfileManager();

        new PluginSubscriber(this);

        getCommand("webregister").setExecutor(new WebRegisterCommand(this));
    }

    public void onDisable() {
        try {
            this.sqlManager.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        VerifyPlugin.instance = null;
    }

    private void loadConfigs() {
        this.saveDefaultConfig();

        File messageConfigFile = new File(this.getDataFolder(), "messages.yml");
        if (!messageConfigFile.exists()) {
            this.saveResource("messages.yml", false);
        }

        this.messageConfig = YamlConfiguration.loadConfiguration(messageConfigFile);
    }
}