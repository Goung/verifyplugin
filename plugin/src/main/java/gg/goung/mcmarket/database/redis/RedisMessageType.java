package gg.goung.mcmarket.database.redis;

public enum RedisMessageType {
    UPDATE_PLAYER,
    UPDATE_NAME,
    PLAYER_LINKED
}
