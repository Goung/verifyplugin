package gg.goung.mcmarket;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class Config {
    public static final List<String> UNUSED_CODE;
    public static final List<String> ALREADY_LINKED;
    public static final List<String> CREATED_CODE;

    static {
        FileConfiguration config = VerifyPlugin.getInstance().getMessageConfig();
        UNUSED_CODE = config.getStringList("unused-code");
        ALREADY_LINKED = config.getStringList("already-linked");
        CREATED_CODE = config.getStringList("created-code");
    }
}
